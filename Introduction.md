<p align="center"> <img src="images/foss-logo.png" alt="FOSSNSS" width="400"/> <br> </p>

# <p align="center"> Welcome to FOSSNSS Membership Drive!</p>
-------

###### The Free and Open Source Software cell of NSS College of Engineering, Palakkad Kerala, India


### About FOSNSS

__FOSSNSS__ is a collective of students to bring out freedom through inspiration,
building skills for collaboration and spreading of FOSS including its ideology.

Our team has been constantly working for the promotion of free and open source technology tools, develop software that respects users freedom, we try our best to bring an inclusive culture in campus for having strong discussion about open source, latest technology and anything that falls under freedom.

#### We believe in Free software, Free Campus, Free Society.
----

### Why membership drive?

Even though we want every free and open source enthusiast to get engaged in our community, we like to build a vibrant team to take up the lead, bring cool initiatives and uphold the FOSS ideology in heart.

The folks who completes the membership tasks will drive the future of FOSSNSS. Our events, initiatives, discussions everything stays open for all.

Please note that, our cell not only promotes just code contribution, but also seeks and promotes technology enthusiasts from all walks of life.

From designers to content writers, open-source communities needs all kinds of individuals! Definetely everyone can get involved in one way or the other.

----
### Important notes

- Before proceeding make sure you have filled the form by clicking [**Become a member**](https://fossnss.org) in our website.

- Read our [**Code of Conduct**](https://fossnss.org/codeofconduct). Before proceeding to next step, we **assume you agree to follow the same**.

- Anyone with basic internet connectivity and a computer can complete the membership tasks.

- This is not a competition, nor any certificate will be issued. We will invite the completed students to our team, so everyone who completes will be accepted as a **"FOSSNSS Member"**.

- Checkout our **website** at https://fossnss.org/ for more information.

----
### FAQ

**Q: I'm not a student from NSS College of Engineering, am I elegible to participate?**

A: No. Even though FOSSNSS welcomes everyone to get engage with FOSS Community, membership drive is for the students from campus.
If you are not from NSSCE, leave an email at foss@nssce.ac.in.

**Q: I have no prior programming experience, and don't know what exactly is FOSS. Can I still complete the tasks?**

A: The tasks are for anyone who has interest in joining our community. No coding/ programming experience is required. 
Regarding what is FOSS, checkout the Wikipedia article [here]("https://en.wikipedia.org/wiki/Free_and_open-source_software").

**Q: Which branch/ year can complete this tasks?**

A: Our primary goal with this membership drive is to invite first years to the team, **irrespective of any branches**. If you are from sernior year, just mention during the process.

**Q: What are the perks being a FOSSNSS Member?**

A: Being a valued member of our team, you will get the chance to drive the FOSSNSS Community, working closely with the core team members. We need to build a vibrant team to support us, bring in quality initiatives, and most importantly **learn together**. 

**Q: Where to contact for additional help?**

A: We are always happy to answer your queries at foss@nssce.ac.in or start a new issue [here](https://gitlab.com/fossnss/fossnss-membership/-/issues).

---

**Our FOSS Cell is one of the finest campus community promoting free and open source culture across Kerala. Our members have achievements like getting accepted to Google Summer of Code, having meaningful open source projects and contributions. We also have a lot of mentors available from impactful open source organisations like Debian, Free Software Foundation. We do want to make sure every one in the team has the opportunity to learn and grow.**

----

### **Our associated groups:**


#### **OpenDesign** - for design enthusiasts
<p align="left"> <img src="images/opendesign.png" alt="FOSSNSS" width="200"/></p>

#### **HackerSpace** by HackClub - for hackers who seek to do creative hacks

<p align="left"> <img src="images/hasp.png" alt="FOSSNSS" width="200"/></p>

### Buckle up and follow next steps to join our team, its simple!